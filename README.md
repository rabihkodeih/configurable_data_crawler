# Configurable X-Path Headless Scraper for Job Boards


This is a configurable web crawler based on x-path and headless chrome.


## Installation

First make sure that `Python3`, `pip3` and `virtualenv` are all installed and working fine:

    apt-get update
    apt-get dist-upgrade
    apt-get install -y python3-dev virtualenv gcc libmysqlclient-dev

Note that you need to have **python 3.6.5** installed as the scrapper will not work with **python 3.7**.

Clone the repository into a destination directory, cd into it then create your virtual env using

    virtualenv -p python3 env
    
and activate it by

    . env/bin/activate
    
Now you can install the requirements by

    pip3 install -r requirements.txt
        

## Crawling Job Sites

Issue the following command:

    scrapy crawl jobs -a input_path="./input.json" -o output.json

This will crawl the site according to data in the input file `input.json` and the resultant job items will be saved in an output file `output.json`. Note that the output file will be appended to.

The input file is a json-file and has the following form:

    [
        {
            "urls": ["{url 1}", "{url 2}", ...],
            "headers": {custom headers to send per request, might specify user agent etc...},  # (optional)
            "imports": "from datetime import datetime;from datetime import timedelta",  # (optional)
            "max_items": {maximum number of items to scrap},  # (optional, default 0 "infinite")
            "dont_filter": {enable/disable duplicate url filtering},  # (optional, 1|0 default 0)
            "next_page_url": "{xpath-selector of DOM element holding the next-page url link}",  # (optional)
            "next_page_btn": "{xpath-selector of DOM element holding the next-page button}",  # (optional)
            "clear_after_click": {wether to clear wait_selector elements after clicking next page button},  # (optional, 1|0 default 0)
            "content_selector": "{xpath-selector of main DOM element holding the job item}",
            "meta_{field 1}": "{value of meta_{field 1}}",
            "meta_{field 2}": "{value of meta_{field 2}}",
            ...
            "job_{field 1}": "xpath-selector of DOM element of job_{field 1}}",
            "job_{field 2}": "xpath-selector of DOM element of job_{field 2}}",
            ...
        },
        ...
    ]

For each json object in the list, the crawler will loop through all the urls in `"urls"`. For each one, it will evaluate `"content_selector"` relative to 
the root page node and loop through the resulting nodes. These nodes correspond to the DOM elements of each job item.

Job fields designated by `job_{field name}` will be scraped using the specified xpath selector. Note that this selector is evaluated relative to the job content node. There is also the option of appending a python lambda expression to the selector, in which case the expression will applied to the evaluated
selector. For example:

    ...
    "job_city": "span[@class='location']/text():::lambda x:x.split()[0]",
    "job_country": "span[@class='location']/text():::lambda x:x.split()[-1]",
    ...

In case the lambda expression needed a certain module, the module can be imported using the `"imports"` argument, ex:

    ...
    "imports": "from datetime import datetime;from datetime import timedelta",
    ...

Additionaly, xpath-selector can also be a list of selectors. In such case the crawler will treat all the selectors except the last one as urls to be visited (in the listed order) 
and then will evaluate the last selector in the list to get the field value.

Meta job fields designated by `meta_{field name}` are just fields to hold job related meta-data and will be filled with the specified values.

Argument `"max_items"` is used mostly to limit the number of items scrapped, useful in debugging scenarios. `"dont_filter"` is used to specify wether or not 
the spider should crawl urls that have been previously visited, sometimes this is required in lazy scenarios (see below).

Finally, if the job listing is paginated, the crawler will move on to additional pages using `"next_page_url"` (or `"next_page_btn)"`, see below)`.


## Example

`input.json`:

    [
        {
            "urls": ["https://jobs.hilton.com/search/?country=UnitedArabEmirates&resultsLanguage=en"],
            "content_selector": "//div[@id='searchResultsContainer']/a",
            "meta_org": "Hilton", 
            "meta_lang": "en",
            "job_title": "div[@class='jobDescription_h']/text()",
            "job_location": "div[contains(@class,'sr_location_txt')]/text()",
            "job_brand": "div[contains(@class,'sr_brand_txt')]/text()"         
        },
        {
            "urls": ["https://careers.hilti.com/en/search/jobs/usa",
                     "https://careers.hilti.com/en/search/jobs/france"],
            "content_selector": "//article[@class='search-result']",
            "next_page_url": "//li[@class='pager-next']/a/@href",
            "meta_org": "Hilti", 
            "meta_lang": "en",
            "job_title": "h3/a/text()",
            "job_location": "div[@class='location']/text()",
            "job_description": "p/text()",
            "job_requirements": ["div/a[@class='read-more']/@href", "//div[@class='field--job-requirements']/node()"]       
        }
    ]


## Building Xpath Selectors In Practice

To ease the task of building the xpath selectors, run:

    scrapy shell
    fetch("{url to be crawled}")

Open your browser dev-tools, inspect the elements you want to find then:

    response.xpath("{xpath expression")
    response.xpath("{xpath expression").extract()
    response.xpath("{xpath expression").extract()_first()
    
For more information, see [Scrapy Shell](https://doc.scrapy.org/en/latest/topics/shell.html).


## Crawling Sites with Ajax DOM Updates

Sometimes a job board site will run some javascript on page load to fetch the desired data. In such case, if we try to run the regular crawler, we wont find any data.
It is therefore necessary to tell the spider to wait for the site to finish rendering until data is ready to be scraped. 

The rule is that any url value (direct or xpath selector) can be appended by a "wait for element to appear in DOM" selector:

    [
        {
            "urls": ["{<xpath_selector_of_DOM_element_to_wait_for>~<url 1>}", 
                     "{<xpath_selector_of_DOM_element_to_wait_for>~<url 2>}", 
                     ...],
            "max_items": {maximum number of items to scrap},  # (optional)
            "content_selector": "{xpath-selector of main DOM element holding the job item}",
            "next_page_url": "{<xpath_selector_of_DOM_element_to_wait_for>~<xpath-selector of next-page url link>}",  # (optional)
            "meta_{field 1}": "{value of meta_{field 1}}",
            "meta_{field 2}": "{value of meta_{field 2}}",
            ...
            "job_{field 1}": "xpath-selector of DOM element of job_{field 1}}",
            "job_{field 2}": "xpath-selector of DOM element of job_{field 2}}",
            ...
        },
        ...
    ]

What this does is to tell the spider to wait for for the element (specified by `<xpath_selector_of_DOM_element_to_wait_for>`) to appear in the DOM prior to returning the html page for parsing.
The page is rendered in this case using a headless browser (splash) instead of the usual get request methods (which do not run any javascript). The same applies for `next_page_url`
and job fields (applicable only to all before last element of selectors list). 

Note that `{<xpath_selector_of_DOM_element_to_wait_for>` is optional in which case the headless browser will wait for 0.5 seconds before returning the rendered html page.

Example:

    [
        {
            "urls": ["//li[@class='job-result']~https://careers.fedex.com/express/jobs"],
            "max_items": 35,
            "content_selector": "//li[@class='job-result']",
            "next_page_url": "//li[@class='job-result']~//a[@class='page-link next']/@href",
            "meta_org": "Fedex",
            "meta_lang": "en",
            "job_title": "div[1]//span[@itemprop='title']/text()",
            "job_requisition_id": ["~div[@class='read-more')]/a/@href", "//h5[@class='job-description-reqid']/text()"]
        }
    ]


## Handling Javascript Pagination Buttons 

Some job board sites (ex: Weber Shandwick) do not have a next page link with an anchor having a url. In this case we have to simulate a button click on 
the corresponding next-page button. In order to do this, we use the `"next_page_btn"` argument, for example:

    [
        {
            "meta_org": "Weber Shandwick",
            "urls": ["//tbody[@class='jobsbody']/tr~https://ipg.taleo.net/careersection/ws_ext_cs/jobsearch.ftl"],
            "max_items": 0,
            "dont_filter": 1,
            "clear_after_click": 1,
            "content_selector": "//tbody[@class='jobsbody']/tr",
            "next_page_btn": "//tbody[@class='jobsbody']/tr~//span[@class='pagerlink']/a[@id='next']",
            "job_title": "th/div/div/span/a/text()",
            "job_location": "td[2]/div/div/span/text()"
        }
    ]

Note that above the `"next_page_btn"` argument contains a "wait for" portion since the page update happens through AJAX. It is the same expression used in `"content_selector"`
. Also note that it is necessary to set `"dont_filter"` to 1 in order for pagination to work (since we are crawling the same url over and over). The option
`"clear_after_click"` is sometimes used to clear wait elements right after clicking the next page button; this might be necessary in some sites which use
reactive UI updates.


## Logging

After any scrapping operation in production, log files should be inspected for errors and warnings. If no log file is specified, logging will be directed to
the standard output. To log to file use:

    scrapy crawl jobs -a input_path="./input.json" -o output.json --logifile <log_path> -L <log_level>
    
`log_path` and `log_level` define the file path and minimim level of messages to log. There are five levels of logging listed in decreasing order:

    CRITICAL
    ERROR
    WARNING
    INFO
    DEBUG

For example if a level of `WARNING` is specified only log message of that level and above will be logged (`ERROR` and `CRITICAL`).

### Log Warnings

The jobs spider will issue log warnings on the following conditions

#### 1) (DROPPED_DUPLICATE_URL) Dropped items due to duplicate url filtering 
Format:

    [log message]\n
    [crawl parameters]\n
    [dropped item]

Example:
    
    2018-10-02 18:08:03 [jobs] WARNING: (DROPPED_DUPLICATE_URL) Item dropped due to url duplicate filtering:
    {'urls': ['http://quotes.toscrape.com/tag/love/'], 'content_selector': "//div[@class='quote']", 'max_items': 0, 'dont_filter': 0, 'next_page_url': "//li[@class='next']/a/@href", 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': "span/small[@class='author']/text()", 'job_quote': "span[@class='text']/text()", 'job_dob': ['span/a/@href', "//span[@class='author-born-date']/text()"], 'job_lob': ['span/a/@href', "//span[@class='author-born-location']/text()"]}
    {'url': 'http://quotes.toscrape.com/tag/love/', 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': 'Marilyn Monroe', 'job_quote': '“If you can make a woman laugh, you can make her do anything.”'}

#### 2) (ABORT_MAX_ITEMS_REACHED) Maximum number of scrapped items reached
Format:
    
    [log message]\n
    [crawl parameters]

Example:

    2018-10-02 18:12:37 [jobs] WARNING: (ABORT_MAX_ITEMS_REACHED) Aborted, maximum item count exceeded:
    {'urls': ['http://quotes.toscrape.com/tag/love/'], 'content_selector': "//div[@class='quote']", 'max_items': 3, 'dont_filter': 1, 'next_page_url': "//li[@class='next']/a/@href", 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': "span/small[@class='author']/text()", 'job_quote': "span[@class='text']/text()", 'job_dob': ['span/a/@href', "//span[@class='author-born-date']/text()"], 'job_lob': ['span/a/@href', "//span[@class='author-born-location']/text()"]}

#### 3) (EMPTY_FIELDS) Some of the scrapped item fields are empty 
Format:

    [log message]\n
    [crawl parameters]\n
    [dropped item]

Example:

    2018-10-02 18:14:28 [jobs] WARNING: (EMPTY_FIELDS) Some item fields are empty:
    {'urls': ['http://quotes.toscrape.com/tag/love/'], 'content_selector': "//div[@class='quote']", 'max_items': 0, 'dont_filter': 1, 'next_page_url': "//li[@class='next']/a/@href", 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': "span/small[@class='authr']/text()", 'job_quote': "span[@class='text']/text()", 'job_dob': ['span/a/@href', "//span[@class='author-born-date']/text()"], 'job_lob': ['span/a/@href', "//span[@class='author-born-location']/text()"]}
    {'url': 'http://quotes.toscrape.com/tag/love/', 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': '', 'job_quote': '“There is nothing I would not do for those who are really my friends. I have no notion of loving people by halves, it is not my nature.”', 'job_dob': 'December 16, 1775', 'job_lob': 'in Steventon Rectory, Hampshire, The United Kingdom', 'parsed_at': '2018-10-02T18:14:28.182594+03:00'}

#### 4) (DROPPED_SPIDER_ERROR) Dropped Items due to general spider error (timeouts, unreachable host, DNS lookup failure, etc...)
Format:

    [log message]\n
    [request url]\n
    [crawl parameters]\n
    [dropped item]\n
    [error traceback dump]
    
Example:

    2018-10-03 23:47:05 [jobs] WARNING: (DROPPED_SPIDER_ERROR) Item dropped due to a spider error:
    http://quotes.toscrape.com/tag/love/
    {'urls': ['http://quotes.toscrape.com/tag/love/'], 'content_selector': "//div[@class='quote']", 'max_items': 0, 'dont_filter': 1, 'headers': {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15'}, 'next_page_url': "//li[@class='next']/a/@href", 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': "span/small[@class='author']/text()", 'job_quote': "span[@class='text']/text()", 'job_dob': ['span/a/@href', "//span[@class='author-born-date']/text()"], 'job_lob': ['span/a/@href', "//span[@class='author-born-location']/text()"]}
    {'url': 'http://quotes.toscrape.com/tag/love/', 'meta_org': 'Test Case', 'meta_lang': 'en', 'job_author': 'Alfred Tennyson', 'job_quote': '“If I had a flower for every time I thought of you...I could walk through my garden forever.”'}
    Traceback (most recent call last):
    --- <exception caught here> ---
      File "/Users/rabihkodein/Documents/Clients/esanjo/camel_scrap/env/lib/python3.6/site-packages/scrapy/core/downloader/middleware.py", line 43, in process_request
        defer.returnValue((yield download_func(request=request,spider=spider)))
    twisted.internet.error.NoRouteError: No route to host: 51: Network is unreachable.
    
#### 5) (DROPPED_MISSING_SCHEMA_FIELD) Dropped Items due to a missing schema field
Format:

    [log message]\n
    [source url]\n
    [missing field]\n
    [dropped item]
    
Example:

    2018-10-25 18:46:36 [jobs] WARNING: (DROPPED_MISSING_SCHEMA_FIELD) Item dropped, schema field not found:
    https://boards.greenhouse.io/careem
    extra
    {'url': 'https://boards.greenhouse.io/careem', 'meta_format_published_on': 'generic', 'job_company_logo': 'https://www.careem.com/images/logos/header/favicon-0_0.png', 'job_industry': 'Technology, Transportation, Trucking & Railroad, Food Tech', 'job_company': 'careem', 'job_title': 'General Manager CareemPay', 'job_location': 'Amman', 'job_city': 'Amman', 'job_country': 'n/a', 'job_application_url': 'https://boards.greenhouse.io/careem/jobs/4103757002', 'job_function': 'Careem Pay', 'job_published_on': 'n/a', 'job_job_type': 'n/a', 'job_seniority_level': 'n/a', 'job_remote': 'n/a'}

#### 6) (DROPPED_DISALLOWED_REGION) Dropped Items that are not in allowed region
Format:

    [log message]\n
    [source url]\n
    [dropped item country]\n
    [dropped item]
    
Example:

    2018-10-25 18:49:32 [jobs] WARNING: (DROPPED_DISALLOWED_REGION) Item dropped, country not in allowed region:
    https://boards.greenhouse.io/careem
    n/a
    {'url': 'https://boards.greenhouse.io/careem', 'meta_format_published_on': 'generic', 'job_company_logo': 'https://www.careem.com/images/logos/header/favicon-0_0.png', 'job_industry': 'Technology, Transportation, Trucking & Railroad, Food Tech', 'job_company': 'careem', 'job_title': 'Senior GL Acccountant', 'job_location': 'Karachi', 'job_city': 'Karachi', 'job_country': 'n/a', 'job_application_url': 'https://boards.greenhouse.io/careem/jobs/4043535002', 'job_function': 'Trust & Safety', 'job_published_on': 'n/a', 'job_job_type': 'n/a', 'job_seniority_level': 'n/a', 'job_remote': 'n/a'}

#### 7) (DROPPED_INVALID_DATE) Dropped Items that resulted in bad date parsing
Format:

    [log message]\n
    [source url]\n
    [date field name]\n
    [date field value]\n
    [date field format]\n
    [dropped item]
    
Example:

    2018-10-25 18:49:32 [jobs] WARNING: (DROPPED_INVALID_DATE) Item dropped, invalid date parsing:
    https://www.linkedin.com/jobs/search/?f_C=2509602&locationId=OTHERS.worldwide
    job_published_on
    2018-09-25T17:38:42.305216
    %d/%m/%Y
    {'url': 'https://www.linkedin.com/jobs/search/?f_C=2509602&locationId=OTHERS.worldwide', 'meta_format_published_on': '%d/%m/%Y', 'job_company_logo': 'https://media.licdn.com/dms/image/C4D0BAQFkQxda5qYiVg/company-logo_100_100/0?e=1548288000&v=beta&t=cYRt8XA4SsPuk51oXgtsm0UZqBo-Ene559Doyzin9r0', 'job_industry': 'Technology, E-Commerce, Apparel & Fashion', 'job_company': 'namshi', 'job_title': 'Chief Accountant', 'job_location': 'Dubai, Dubai, United Arab Emirates', 'job_city': 'Dubai', 'job_country': 'United Arab Emirates', 'job_application_url': 'https://www.linkedin.com/jobs/view/chief-accountant-at-namshi-com-877408580?trkInfo=searchKeywordString%3A%2CsearchLocationString%3A%252C%2B%2Cvertical%3Ajobs%2CpageNum%3A0%2Cposition%3A5%2CMSRPsearchId%3A8959cce2-b27c-4683-9b9d-277b0eaf9046&refId=8959cce2-b27c-4683-9b9d-277b0eaf9046&trk=jobs_jserp_job_listing_text', 'job_published_on': '2018-09-25T17:38:42.305216', 'job_remote': 'false'}


### Detecting Forbidden Responses by ROBOTS.TXT

Some job boards might prevent scrapping alltogether using rules encoded into their robots.txt file. To detect forbidden requests, scan the logs for the following message format:

    yyyy-mm-dd hh:mm:ss [scrapy.downloadermiddlewares.robotstxt] DEBUG: Forbidden by robots.txt: <GET [url of website>

Example:

    2018-10-03 21:49:54 [scrapy.downloadermiddlewares.robotstxt] DEBUG: Forbidden by robots.txt: <GET https://www.netflix.com>


## Autothrottling of Requests Rate

Because some job board servers cannot handle high ammounts of traffic (or even worse, shutt out bot scrapping based on rate of requests) it is highly advisable to use request rate autothrottling by setting `AUTOTHROTTLE_ENABLED` to `True` (`False` by default) in `settings.py`. The different parameters have been set to reasonable values after some experimentation:

    ...
    # Enable and configure the AutoThrottle extension (disabled by default)
    # See https://doc.scrapy.org/en/latest/topics/autothrottle.html
    AUTOTHROTTLE_ENABLED = False
    #  The initial download delay
    AUTOTHROTTLE_START_DELAY = 1
    #  The maximum download delay to be set in case of high latencies
    AUTOTHROTTLE_MAX_DELAY = 20
    #  The average number of requests Scrapy should be sending in parallel to
    #  each remote server
    AUTOTHROTTLE_TARGET_CONCURRENCY = 4.0
    #  Enable showing throttling stats for every response received:
    AUTOTHROTTLE_DEBUG = False
    ...


## Custom Headers

To specify a custom header in the scrapping requests, set the `"headers"` key in crawling arguments. 
For example, to change the user agent: (default user agent is `"job_crawler (+https://www.job_crawler.io)"`)

    {
        "urls": ["http://quotes.toscrape.com/tag/love/"],
        "content_selector": "//div[@class='quote']",
        "max_items": 0,
        "dont_filter": 1,
        "headers": {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15"},
        "next_page_url": "//li[@class='next']/a/@href",
        "meta_org": "Test Case",
        "meta_lang": "en",
        "job_author": "span/small[@class='author']/text()",
        "job_quote": "span[@class='text']/text()",
        "job_dob": ["span/a/@href", "//span[@class='author-born-date']/text()"],
        "job_lob": ["span/a/@href", "//span[@class='author-born-location']/text()"]
    }


## Using Docker

Clone the repository into a project source folder, then:

    cd <project source folder>
    docker build -t cameljobs_crawl .
    docker run --name worker -itd cameljobs_crawl:latest
    
    
Then:

    docker exec -it worker /bin/bash
    python tests/run_tests.py info
    
to make sure that the scrapper is working correctly.
    

## Runing Test Cases

From the main project folder run:

    python tests/run_tests.py info













